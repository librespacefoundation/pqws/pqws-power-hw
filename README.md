# PQ9 Basic Power Module

This is a basic power module for PQ9ISH applications  

It provides the following voltages:  

V1: 3.3V 1A  
V2: 5V 1A  

## Pinout

![](power.png)